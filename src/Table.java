import java.io.File;
import java.security.KeyStore;
import java.util.*;

public class Table {
    private static final int DIM = 4;
    private static final int SIZE = DIM * DIM;
    static final int[] winState = new int[SIZE - 1];
    private static String[][] mainBoardGame = new String[3][3];
    private static ArrayList<Integer> winStat = new ArrayList<>();
    private static ArrayList<Integer> dynamic = new ArrayList<>();
    private static String[] Temp = new String[16];
    static int emptyCell = DIM * DIM;
    private static int iOfMovingCell = 0;
    private static int jOfMovingCell = 0;


    public static List<Integer> FifteenPuzzle() {

        for (int i = 1; i < SIZE; i++) {
            winState[i - 1] = i;
        }
        for (int i : winState) {
            winStat.add(i);
        }
        return winStat;
    }

    public static void moveRight(ArrayList<Integer> board) {
        if (emptyCell == 0 || emptyCell == 4 || emptyCell == 8 || emptyCell == 12)
            System.out.println("invalid move !");
        else {
            int movingCell = emptyCell - 1;
            Collections.swap(board, emptyCell, movingCell);
            setEmptyCell(board);
            System.out.println("The emptycell : " + emptyCell);
        }


    }


    public static void moveLeft(ArrayList<Integer> board) {
        if (emptyCell == 3 || emptyCell == 7 || emptyCell == 11 || emptyCell == 15)
            System.out.println("invalid move !");
        else {
            int movingCell = emptyCell + 1;
            Collections.swap(board, emptyCell, movingCell);
            setEmptyCell(board);
            System.out.println("The emptycell : " + emptyCell);
        }


    }


    public static void moveUp(ArrayList<Integer> board) {
        if (emptyCell == 12 || emptyCell == 13 || emptyCell == 14 || emptyCell == 15)
            System.out.println("invalid move !");
        else {
            int movingCell = emptyCell + 4;
            Collections.swap(board, emptyCell, movingCell);
            setEmptyCell(board);
            System.out.println("The emptycell : " + emptyCell);
        }

    }

    public static void moveDown(ArrayList<Integer> board) {
        if (emptyCell == 0 || emptyCell == 1 || emptyCell == 2 || emptyCell == 3)
            System.out.println("invalid move !");
        else {
            int movingCell = emptyCell - 4;
            Collections.swap(board, emptyCell, movingCell);
            setEmptyCell(board);
            System.out.println("The emptycell : " + emptyCell);
        }

    }

    public static void convertArraylistToTable(ArrayList<Integer> arrayList) {
        for (int i = 0; i < 4; i++) {
            if (arrayList.get(i) < 10) {
                if (arrayList.get(i) == 0) System.out.print("  ");
                else System.out.print(" " + arrayList.get(i) + " ");
            } else
                System.out.print(arrayList.get(i) + " ");
        }
        System.out.println();
        for (int i = 4; i < 8; i++) {
            if (arrayList.get(i) < 10) {
                if (arrayList.get(i) == 0) System.out.print("  ");
                else System.out.print(" " + arrayList.get(i) + " ");
            } else
                System.out.print(arrayList.get(i) + " ");
        }
        System.out.println();
        for (int i = 8; i < 12; i++) {
            if (arrayList.get(i) < 10) {
                if (arrayList.get(i) == 0) System.out.print("  ");
                else System.out.print(" " + arrayList.get(i) + " ");

            } else
                System.out.print(arrayList.get(i) + " ");
        }
        System.out.println();
        for (int i = 12; i < 16; i++) {
            if (arrayList.get(i) < 10) {
                if (arrayList.get(i) == 0) System.out.print("  ");
                else System.out.print(" " + arrayList.get(i) + " ");
            } else
                System.out.print(arrayList.get(i) + " ");
        }
        System.out.println();


    }

    public static void setEmptyCell(ArrayList<Integer> intialList) {
        for (int index = 0; index < SIZE; index++) {
            if (intialList.get(index) == 0) {
                emptyCell = index;
            }
        }
    }

    public static ArrayList<Integer> starterBoard() {
        ArrayList<Integer> intialList = new ArrayList<Integer>(SIZE);
        intialList = new ArrayList<Integer>(SIZE);
        for (int i = 0; i < SIZE; i++) {
            intialList.add(i, i);
        }

        Collections.shuffle(intialList);

        for (int index = 0; index < SIZE; index++) {
            final int ROW = index / DIM;  // row number from index
            final int COL = index % DIM;   // column number from index

            // Initializes the empty square and hide it
            if (intialList.get(index) == 0) {
                emptyCell = index;
            }

        }
        return intialList;


    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        ArrayList<Integer> intialList = new ArrayList<Integer>(SIZE);
        intialList = starterBoard();
        System.out.println(intialList);
        System.out.println(intialList.get(2));
        System.out.println(emptyCell);
        convertArraylistToTable(intialList);
        char move;
        boolean notEnd = true;
        while (notEnd) {
            if (intialList == winStat) notEnd = true;
            else {
                move = input.next().charAt(0);
                if (move == 'a') {
                    moveLeft(intialList);
                    convertArraylistToTable(intialList);
                } else if (move == 'd') {
                    moveRight(intialList);
                    convertArraylistToTable(intialList);
                } else if (move == 'w') {
                    moveUp(intialList);
                    convertArraylistToTable(intialList);
                } else if (move == 's') {
                    moveDown(intialList);
                    convertArraylistToTable(intialList);
                } else break;
            }
        }


        boolean Continue = true;


    }


}
